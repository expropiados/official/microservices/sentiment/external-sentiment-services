from script import script

class dialogo:
    def __init__(self):
        self.nspeakers = 0
        self.speakers = [] #lista de speakers, son strings
        self.scripts = [] # lista de scripts
        self.complete_text = ""

    def addScript(self, line):
        self.scripts.append(line)

    def populate(self, outResp):
        self.nspeakers = 1
        segmentosProcesados = []
        # print('outResp:: populate', outResp)
        self.complete_text = outResp['results']['transcripts'][0]['transcript']
        segment = []
        for item in outResp['results']['items']:
            if item['type'] == 'punctuation' and item['alternatives'][0]['content'] == '.':
                segmentosProcesados.append(segment)
                segment = []
            elif item['type'] != 'punctuation':
                word = {
                    'word' : item['alternatives'][0]['content'],
                    'start_time': item['start_time'],
                    'end_time': item['end_time'],
                    'confidence' : item['alternatives'][0]['confidence']
                }
                segment.append(word)

        self.scripts = segmentosProcesados