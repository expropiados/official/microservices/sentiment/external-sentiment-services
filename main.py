from typing import Optional
from datetime import datetime
from fastapi import BackgroundTasks, FastAPI, File, UploadFile

from models.text_analysis import analyze_text
from models.text_analysis import load_file
from pruebaTranscript import *
from models.text_analysis import job_analyze_text
from pydantic import BaseModel
class TranscribeJob(BaseModel):
    transcription_id: str
    videocall_dir: str
    user_id: str

app = FastAPI()


@app.get("/")
def read_root():
    return "El Psy Congroo!"


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

@app.get("/text/analysis/{item_id}")
def read_item(item_id: int):
    filename = 'test/text{}.txt'.format(item_id)
    text = load_file(filename)
    analysis = analyze_text(text)
    return analysis

@app.get("/text/transcript/{item_id}")
def read_item(item_id: int):
    filename = 'test/text{}.txt'.format(item_id)
    text = load_file(filename)
    return text

@app.post("/obtainTranscript")
def obtainTranscript(file: UploadFile = File(...), instant:str = None):
    print("HOLA")
    #file es tipo SpooledTemporaryFile
    #print(type(file))
    #audioFile = file.file
    #audioFile = tempfile.SpooledTemporaryFile(file.read())

    print(type(file))
    print(file.filename)
    instante = instant
    instante = (datetime.now()).strftime("%y%m%d%H%M%S")
    status = executeTranscript(instante, file)
    url = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
    print(url)
    mensaje = procesarUrlRecibida(url)
    return mensaje

@app.post("/jobtranscribe")
def obtainTranscript(transcribeJob: TranscribeJob, background_tasks: BackgroundTasks):
    print("TranscriptionID::",transcribeJob.transcription_id)

    dirs = transcribeJob.videocall_dir.split('&')
    print("Videocall Directory", dirs[0])
    dual_videocall_dir = None
    if len(dirs) > 1:
        print("Dual Videocall Directory", dirs[1])
        dual_videocall_dir = dirs[1]

    # start background_task
    background_tasks.add_task(waitTranscription, transcribeJob.transcription_id, dirs[0], transcribeJob.user_id, dual_videocall_dir)
    
    mensaje = 'Job started'
    return {'status': mensaje}

@app.post("/jobanalysis")
def obtainTranscript(transcription_id: str, transcription_url: str, background_tasks: BackgroundTasks):
    print("TranscriptionID::",transcription_id)
    print("AudioURL", transcription_url)

    # start background_task
    background_tasks.add_task(job_analyze_text, transcription_id, transcription_url)
    result = "Job started!"

    # result = job_analyze_text(transcription_id, transcription_url)
    
    return result