from copy import Error
import time
import json
import boto3
import os
import pandas as pd

import http.client
import requests

from configurations import *
from dialogo import *

HOST_JAVAMS = os.environ.get('HOST_JAVAMS')

def parse_words(trans, speaker):
    all_words = []
    all_time = []
    all_speakers = []
    for line in trans:
        for word in line:
            all_words.append(word['word'])
            all_time.append(word['start_time'])
            all_speakers.append(speaker)
    return pd.DataFrame.from_dict({'word': all_words, 'time':all_time, 'speaker':all_speakers})

def group_dialogue(df_dialogue):
    dialogue = df_dialogue.values
    res = []
    prev_spkr = None
    line = ''
    start_time = None
    for word in dialogue:
        if prev_spkr is None:
            line = word[0]
            start_time = word[1]
            prev_spkr = word[2]
            
        elif prev_spkr == word[2]:
            line = line + ' ' + word[0]
            
        else:
            obj = {'speaker': prev_spkr, 'text': line, 'time': start_time}
            res.append(obj)
            line = word[0]
            start_time = word[1]
            prev_spkr = word[2]
    obj = {'speaker': prev_spkr, 'text': line, 'time': start_time}
    res.append(obj)
    return res

def process_videocall(dir_name, user_id, output_dir, dual_videocall_dir=None):
  work_dir = os.path.join(output_dir, dir_name)
  if dir_name in os.listdir(output_dir):
    stream = os.popen('rm -rf {}'.format(work_dir))
    output = stream.read()
  os.mkdir(work_dir)
  # dowload videocall files
  s3 = boto3.resource(
      service_name='s3',
      region_name='us-east-2',
      aws_access_key_id='AKIASW6HVZXEHAOMP3Z5', # os.environ.get('KEY_ID_CHECA')
      aws_secret_access_key='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' # os.environ.get('SECRET_KEY_CHECA')
  )
  checa_s3 = s3.Bucket('videollamadas')
  objs = checa_s3.objects.filter(Prefix=dir_name)

  for obj in objs:
    checa_s3.download_file( obj.key, os.path.join(work_dir, obj.key.split('/')[-1]))
    #print(obj.key)
    
  # convert audio
  print('Converting audio...')
  stream = os.popen('python2 Cloud_Recording_tools/convert.py -f {} -m 2 -s -p 30 -r 640 360'.format(work_dir))
  output = stream.read()

  # convert video
  print('Converting video...')
  stream = os.popen('python2 Cloud_Recording_tools/convert.py -f {} -m 1 -s -p 30 -r 640 360'.format(work_dir))
  output = stream.read()

  # file result
  files = os.listdir(work_dir)

  audio_files = list(filter(lambda x: x.split('.')[-1] == 'm4a', files))
  patient_audio_file = list(filter(lambda x: user_id in x, audio_files))[0]
  specialist_audio_file = list(filter(lambda x: user_id not in x, audio_files))[0]
  print('specialist_audio_file::', specialist_audio_file)
  specialist_audio_file = os.path.join(work_dir, specialist_audio_file)
  patient_audio_file = os.path.join(work_dir, patient_audio_file)

  # convert audiofile
  stream = os.popen('python2 format_convert.py {} m4a mp3'.format(work_dir))
  output = stream.read()

  files = os.listdir(work_dir)
  audio_files = list(filter(lambda x: x.split('.')[-1] == 'mp3', files))
  patient_audio_file = list(filter(lambda x: user_id in x, audio_files))[0]
  patient_audio_file = os.path.join(work_dir, patient_audio_file)
  specialist_audio_file = list(filter(lambda x: user_id not in x, audio_files))[0]
  specialist_audio_file = os.path.join(work_dir, specialist_audio_file)

  print('patient_audio_file::', patient_audio_file)

  # Download mp4
  if dual_videocall_dir:
    objs = checa_s3.objects.filter(Prefix=dual_videocall_dir)

    for obj in objs:
        if obj.key.split('.')[-1] == 'mp4':
            obj_name = obj.key.split('/')[-1].split('.')[0]
            print('mp4 dual video name::', obj_name)
            checa_s3.download_file( obj.key, os.path.join(work_dir, obj_name + 'dual_video.mp4'))
            print('mp4 video::', obj.key)

    files = os.listdir(work_dir)
    video_files = list(filter(lambda x: x.split('.')[-1] == 'mp4', files))
    patient_video_file = list(filter(lambda x: 'dual_video' in x, video_files))[0]
    patient_video_file = os.path.join(work_dir, patient_video_file)
    print('patient_video_file::', patient_video_file)

  # upload to s3 
  s3 = boto3.resource('s3')
  bucket = s3.Bucket(results_bucket_name)
  key= patient_audio_file.split('/')[-1]
  aux = bucket.Object(key).put(Body=open(patient_audio_file, 'rb'))

  audio_url = "https://results-transcription-dp2.s3.amazonaws.com/%s" % (key)
  print('bucket object audio_url::', audio_url)

  key= specialist_audio_file.split('/')[-1]
  aux = bucket.Object(key).put(Body=open(specialist_audio_file, 'rb'))
  audio_url_specialist = "https://results-transcription-dp2.s3.amazonaws.com/%s" % (key)
  print('bucket object audio_url_specialist::', audio_url_specialist)

  s3 = boto3.resource('s3')
  bucket = s3.Bucket(results_bucket_name)
  key= patient_video_file.split('/')[-1]
  aux = bucket.Object(key).put(Body=open(patient_video_file, 'rb'))

  video_url = "https://results-transcription-dp2.s3.amazonaws.com/%s" % (key)
  print('bucket object video_url::', video_url)

  return audio_url, video_url, audio_url_specialist

def guardar(audioF):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(audio_bucket_name)
    bucket.Object(audioF.filename).put(Body=audioF.file)
    return True

def executeTranscriptionJob(tiempo, audioFile, dir_file_bucket):
    transcribe = boto3.client('transcribe')
    nombreArchivo, extenArchivo = audioFile.filename.split('.')

    job_name = "TranscriptionOf" + tiempo

    transcribe.start_transcription_job(
        TranscriptionJobName=job_name,
        Media={'MediaFileUri': dir_file_bucket},
        MediaFormat=extenArchivo,
        LanguageCode='es-ES',
        Settings={'MaxSpeakerLabels':2,'ShowSpeakerLabels':True}
    )
    while True:
        status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
        print("Not ready yet...")
        time.sleep(5)
    return status

def waitTranscription(transcription_id, videocall_dir, user_id, dual_videocall_dir):

    print('Processing agora files...')
    output_dir = os.path.join(os.getcwd(), 'output')
    audio_url, video_url, audio_url_specialist = process_videocall(videocall_dir, user_id, output_dir, dual_videocall_dir)
    print('Processing agora files DONE')
    print('audio_url Agora::', audio_url)

    # Transcription patient audio
    transcribe = boto3.client('transcribe')
    extenArchivo = audio_url.split('.')[-1]

    job_name = transcription_id
    transcribe.start_transcription_job(
        TranscriptionJobName=job_name,
        Media={'MediaFileUri': audio_url},
        MediaFormat=extenArchivo,
        LanguageCode='es-ES'
        #Settings={'MaxSpeakerLabels':1,'ShowSpeakerLabels':True}
    )
    
    status = transcribe.get_transcription_job(TranscriptionJobName=job_name)

    # Transcription specialist audio
    extenArchivo_sp = audio_url_specialist.split('.')[-1]

    job_name_sp = str(transcription_id)+'_sp'
    transcribe.start_transcription_job(
        TranscriptionJobName=job_name_sp,
        Media={'MediaFileUri': audio_url_specialist},
        MediaFormat=extenArchivo_sp,
        LanguageCode='es-ES'
        #Settings={'MaxSpeakerLabels':1,'ShowSpeakerLabels':True}
    )
    
    status_sp = transcribe.get_transcription_job(TranscriptionJobName=job_name_sp)

    while True:
        status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
        print("Not ready yet...")
        time.sleep(5)

    url = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
    dialogo = procesarUrlRecibida(url)

    while True:
        status = transcribe.get_transcription_job(TranscriptionJobName=job_name_sp)
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
        print("Not ready yet...")
        time.sleep(5)
    
    url_sp = status['TranscriptionJob']['Transcript']['TranscriptFileUri']
    dialogo_sp = procesarUrlRecibida(url_sp)

    # merge dialogues
    print('Merging dialogues...')
    patient = dialogo.scripts
    specialist = dialogo_sp.scripts

    df_patient = parse_words(patient, 'Paciente')
    df_specialist = parse_words(specialist, 'Especialista')
    df_dialogue = pd.concat([df_patient, df_specialist])
    df_dialogue = df_dialogue.astype({'time':float})
    df_dialogue = df_dialogue.sort_values('time')

    res = group_dialogue(df_dialogue)

    
    transcripcion = {
        'transcription': dialogo.scripts, 
        'complete_text': dialogo.complete_text, 
        'audio_url': audio_url, 
        'video_url' : video_url, 
        'transcription_specialist': dialogo_sp.scripts, 
        'complete_text_specialist': dialogo_sp.complete_text,
        'dialogue': res
        }
    #print("Result::", transcripcion)

    # Save result in s3
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(results_bucket_name)
    key= 'results_{}.json'.format(job_name)
    aux = bucket.Object(key).put(Body=(bytes(json.dumps(transcripcion).encode('UTF-8'))))

    result_url = "https://results-transcription-dp2.s3.amazonaws.com/%s" % (key)
    print('bucket object result_url::', result_url)

    # Call service
    try:
        # uncoment before push
        service_url = HOST_JAVAMS + save_transcription_url+'?transcription_id={}&transcription_url={}&job_state=1'.format(job_name, result_url)
        print('calling service::', service_url)
        x = requests.post(service_url)
        print(x.text)
        pass
    except Exception as e:
        print(e)
    
    return transcripcion, result_url

#TODO: delete function
def startTranscriptionJob(transcription_id, videocall_dir, user_id):
    output_dir = os.path.join(os.getcwd(), 'output')
    audio_url, video_url = process_videocall(videocall_dir, user_id, output_dir)
    transcribe = boto3.client('transcribe')
    extenArchivo = audio_url.split('.')[-1]

    job_name = transcription_id
    transcribe.start_transcription_job(
        TranscriptionJobName=job_name,
        Media={'MediaFileUri': audio_url},
        MediaFormat=extenArchivo,
        LanguageCode='es-ES'
        #Settings={'MaxSpeakerLabels':1,'ShowSpeakerLabels':True}
    )
    
    status = transcribe.get_transcription_job(TranscriptionJobName=job_name)
    return status, transcribe, audio_url, video_url

def separar(url):
    groups = url.split('/')
    return '/'.join(groups[:3]), '/'.join(groups[3:])

def procesarUrlRecibida(url):
    print(url)
    conn_str, dir_str = separar(url)
    conn = http.client.HTTPSConnection(conn_str.split('/')[2])
    payload = ''
    headers = {}
    conn.request("GET",
                 url,
                 payload, headers)
    res = conn.getresponse()
    raw_data = res.read()
    encoding = res.info().get_content_charset('utf8')  # JSON default
    outResp = json.loads(raw_data.decode(encoding))
    #mensaje = outResp['results']['transcripts'][0]['transcript']
    #print(outResp)
    conversa = dialogo()
    conversa.populate(outResp)
    return conversa

def executeTranscript(instant, audioFile):
    #GUARDAR AUDIOFILE EN S3, EN UNA CARPETA PARTICULAR
    pudoGuardar = guardar(audioFile)
    dir_file_bucket = url_s3.format(audioFile.filename)

    #INSTANCIAR JOB CON ARCHIVO GUARDADO EN BUCKET
    status = executeTranscriptionJob(instant, audioFile, dir_file_bucket)
    return status